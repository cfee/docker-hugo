# Docker Hugo
Imágen [Docker](https://www.docker.com/) con la versión más reciente del generador de páginas estáticas [Hugo](https://gohugo.io/).

```bash
$ docker images
REPOSITORY    TAG             IMAGE ID       CREATED          SIZE
hugo          alpine          15db1e91898f   11 minutes ago   72.3MB
```

## Descripción
Con el objetivo de implementar un entorno de desarrollo GO + Hugo para distintos proyectos web de este Consejo, y al no encontrar en Internet una opción que se adecue a nuestros lineamientos de desarrollo de software y seguridad, se decidió desarrollar nuestra propia imágen Docker. Esto le brinda al Consejo soberanía y mayor control sobre el stack de tecnologías y la Software Supply Chain utilizada.

## Installación
Aún no existe una imágen de este Dockerfile disponible para su descarga en registros públicos como Docker Hub o Google Container Registry.

Por tal motivo, para poder usarla, esta imágen debe compilarse e instalarse en el repositorio local de imágenes Docker de la PC, por medio del siguiente comando:

```bash
$ docker build -t hugo:alpine .
```

Donde el parámetro `-t hugo:alpine` puede llevar cualquier otro nombre que sirva para identificar univocamente la imágen dentro del sistema Docker.

## Uso

Una vez creada la imágen a partir del Dockerfile, se puede:

* Utilizar la imágen para ejecutar un comando de una sola vez.
* Desplegar un entorno de desarrollo para un sitio web basado en Hugo.
* Interactuar y ejecutar comandos en un contenedor ya en ejecución, basado en esta imágen.

### Ejecutar comandos de una sola vez

```bash
$ docker run --rm IMAGEN:TAG COMANDO ARG1 ARG2 ... ARGn
```

Donde:

* El parámetro `--rm` es para el contenedor se borre una vez ejecutado el comando.
* `IMAGEN:TAG` es el nombre de la imágen y tag que se asignarion al crear la imágen, según se explicó en la sección anterior.
* `COMANDO` es el comando a ejecutar.
* `ARG` es el o los argumentos que opcionalmente se le pueden pasar al comando.

Por ejemplo:

```bash
$ docker run --rm hugo:alpine version
hugo v0.106.0+extended linux/amd64 BuildDate=unknown
```

### Desplegar un entorno de desarrollo

Si no está creado aún, crear un sitio nuevo:

```bash
$ docker run --rm -v $(pwd):/srv hugo:alpine new site /srv/site
```

Finalmente, iniciar el servidor de desarrollo:

```bash
$ docker run --rm -v $(pwd):/srv -w /srv/site -p 1313:1313 -d hugo:alpine serve --bind=0.0.0.0
```

Luego de esto, se puede abrir un navegador en `http://localhost:1313`

## Soporte
Aunque este repositorio es de acceso público, su contenido está previsto para ser utilizado como herramienta de uso interno en este Consejo. Por tal motivo, no se brinda ningún tipo de soporte externo y este software se entrega así como está; es decir, sin ningún tipo de grarantía.

## Roadmap
Features que nos gustaría implementar a futuro:

* Implementar [GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/) y automatizar la creación de la imágen por medio de CI/CD.
* Implementar escaneos automáticos de seguridad via CI/CD.

## Contribuir
Para contribuir en este proyecto, puedes:

* Consultar, informar sobre errores o ayudar a otro usuario, utilizando los issues de este proyecto.
* Sugerir cambios en el código, clonando este repositorio y abriendo un Merge Request para su revisión y posterior aprobación (o no).

## Autores y reconocimientos
Desarrollado por Victor Villarreal (vvillarreal@cfee.gob.ar) para el Consejo Federal de Energía Eléctrica.

## Licencia
Copyright 2023 Consejo Federal de Energía Eléctrica (https://www.cfee.gob.ar).