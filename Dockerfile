FROM alpine:latest

RUN apk add --update-cache --no-cache hugo

EXPOSE 1313

ENTRYPOINT [ "/usr/bin/hugo" ]